<?php

	$date = isset($_GET['date']) ? $_GET['date'] : 'week';

	$date1 = $date;

	if($date1 == 'week') {		// если неделя
		$date1 = date('Ymd',strtotime("-7 day"));
	} elseif($date1 == 'month') {	// если месяц
		$date1 = date('Ymd',strtotime("-1 month"));
	} elseif($date1 == 'quart') {	// если квартал
		$date1 = date('Ymd',strtotime("-3 month"));
	} elseif($date1 == 'year') {	// если год
		$date1 = date('Ymd',strtotime("-12 month"));
	} else {
		$date1 = date('Ymd',strtotime("-1 month"));
	}

	$date2 = date('Ymd');

	$group = isset($_GET['group']) ? $_GET['group'] : 'week';

	if($group == 'day'){
		$group = 'day';
	} elseif($group == 'week') {
		$group = 'week';
	} elseif($group == 'month') {
		$group = 'month';
	} else {
		$group = 'day';
	}


	$url = 'https://api-metrika.yandex.ru/stat/traffic/summary.json?id='.$dl_metrika_id.'&oauth_token='.$dl_token.'&date1='.$date1.'&date2='.$date2.'&group='.$group;
	$json_data = file_get_contents($url);
	$json_data = json_decode($json_data, true); 
?>


<script type="text/javascript">
      google.load("visualization", "1.1", {packages:["bar"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['', 'Визиты', 'Просмотры', 'Посетители'],
<?php
	foreach($json_data['data'] as $key => $value) { 
		
		$__date = date('d.m.y',strtotime($json_data['data'][$key]['date']));
		$visites = $json_data['data'][$key]['visits'];
		$page_views = $json_data['data'][$key]['page_views'];
		$visitors = $json_data['data'][$key]['visitors'];
		
		echo '[\''. $__date .'\','.$visites.','.$page_views.','.$visitors.'],';

	}
?>
        ]);

         var options = {
          chart: {
            title: 'Данные о посещаемости сайта'
          }
        };

        var chart = new google.charts.Bar(document.getElementById('visites_div'));

        chart.draw(data, options);
		
	}
</script>
	

<div class="wrap">
	
	<h2>Отчет Посещаемость <a href="https://metrika.yandex.ru/stat/traffic?id=<?php echo $dl_metrika_id; ?>" target="_blank" style="float: right" class="button">Отчет на Yandex.Metrika</a></h2>

	<div class="wrap">
		<div class="wp-filter" style="margin: 0;">
			<ul class="nav nav-tabs">
				<li><a href="#"><strong>Период</strong></a></li>	
				
				<li <?php if($date == 'quart') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => 'quart', 'group' => $group)); ?>">квартал</a>
				</li>
				<li <?php if($date == 'month') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => 'month', 'group' => $group)); ?>">месяц</a>
				</li>
				<li <?php if($date == 'week') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => 'week', 'group' => $group)); ?>" >неделя</a>
				</li>
				
				<li><a href="#"><strong>Детализация</strong></a></li>	
				
				<li <?php if($group == 'day') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => $date, 'group' => 'day')); ?>">по дням</a>
				</li>
				<li <?php if($group == 'week') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => $date, 'group' => 'week')); ?>">по неделям</a>
				</li>	
				<li <?php if($group == 'month') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => $date, 'group' => 'month')); ?>">по месяцам</a>
				</li>	
			</ul>
		</div>

	    <div class="postbox-container" style="width: 100%">
	        <div class="metabox-holder">
	            <div class="meta-box-sortables">
				
				
                <div class="postbox" id="first">
                    <div class="inside">
						<div id="visites_div" style="width: 98%; height: 250px;"></div>
                    </div>
                </div>
				

				<table class="items table">
					<thead>
					<tr>
						<th class="manage-column column-title"><a>Дата</a></th>
						<th class="manage-column column-author">Визиты</th>
						<th class="manage-column column-author">Просмотры</th>
						<th class="manage-column column-author">Посетители</th>
						<th class="manage-column column-author">Новые посетители</th>
						<th class="manage-column column-author">Глубина просмотра</th>
						<th class="manage-column column-author">Отказы</th>
						<th class="manage-column column-author">Среднее время</th>
					</tr>
					</thead>

					<tbody>
					<?php

					$json_data = array_reverse($json_data['data']);

					foreach($json_data as $key => $value) { 
						$traffic_date 			= $json_data[$key]['date'];
						$traffic_visits 		= $json_data[$key]['visits'];
						$traffic_page_views		= $json_data[$key]['page_views'];
						$traffic_visitors		= $json_data[$key]['visitors'];
						$traffic_depth			= $json_data[$key]['depth'];
						$traffic_new_visitors	= $json_data[$key]['new_visitors'];
						$traffic_denial			= $json_data[$key]['denial'];
						$traffic_visit_time		= $json_data[$key]['visit_time'];
						
						$traffic_visit_time		= $traffic_visit_time/60;
					?>  
					<tr>
					  <th class="manage-column column-title"><a><?php echo date('Y.m.d',strtotime($traffic_date)); ?></a></th>
					  <th class="manage-column column-author"><?php echo $traffic_visits; ?></th>
					  <th class="manage-column column-author"><?php echo $traffic_page_views; ?></th>
					  <th class="manage-column column-author"><?php echo $traffic_visitors; ?></th>
					  <th class="manage-column column-author"><?php echo $traffic_new_visitors; ?></th>
					  <th class="manage-column column-author"><?php echo round($traffic_depth, 1); ?></th>
					  <th class="manage-column column-author"><?php echo round($traffic_denial, 1); ?></th>
					  <th class="manage-column column-author"><?php echo round($traffic_visit_time, 1); ?></th>
					</tr>
					<?php } ?>
					</tbody>
				</table>                        
			</div>
		</div>
	</div>
</div>
</div>