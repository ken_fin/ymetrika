<?php

$date = isset($_GET['date']) ? $_GET['date'] : 'week';

$date1 = $date;

if($date1 == 'week') {		// если неделя
	$date1 = date('Ymd',strtotime("-7 day"));
} elseif($date1 == 'month') {	// если месяц
	$date1 = date('Ymd',strtotime("-1 month"));
} elseif($date1 == 'quart') {	// если квартал
	$date1 = date('Ymd',strtotime("-3 month"));
} elseif($date1 == 'year') {	// если год
	$date1 = date('Ymd',strtotime("-12 month"));
} else {
	$date1 = date('Ymd',strtotime("-7 day"));
}

$date2 = date('Ymd');


$url = 'https://api-metrika.yandex.ru/stat/demography/age_gender.json?id='.$dl_metrika_id.'&oauth_token='.$dl_token.'&date1='.$date1.'&date2='.$date2;
$json_data = file_get_contents($url);
$json_data = json_decode($json_data, true);
?>


<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Пол', 'Процент'],
<?php
foreach($json_data['data_gender'] as $key => $value) { 
	
	$name = $json_data['data_gender'][$key]['name'];
	$visits_percent = $json_data['data_gender'][$key]['visits_percent'];
	
	//$visits_percent = round($visits_percent, 2);
	
	echo '[\''. $name .'\','.$visits_percent.'],';

} ?>
        ]);

      var options = {
		title: 'Пол посетителей',
        pieHole: 0.4,  
		height: 400,
		'chartArea': {'width': '70%', 'height': '70%'},
      };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));
        chart.draw(data, options);
      }
    </script>
<script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Пол', 'Процент'],
<?php
foreach($json_data['data'] as $key => $value) { 
	
	$name = $json_data['data'][$key]['name'];
	$visits_percent = $json_data['data'][$key]['visits_percent'];
	
	//$visits_percent = round($visits_percent, 2);
	
	echo '[\''. $name .'\','.$visits_percent.'],';

} ?>
        ]);

      var options = {
		title: 'Возрастная группа',
        pieHole: 0.4,  
		height: 400,
		'chartArea': {'width': '70%', 'height': '70%'},
      };

        var chart = new google.visualization.PieChart(document.getElementById('datachart'));
        chart.draw(data, options);
      }
</script>	

<div class="wrap">
	<h2>Отчет по полу и возрасту</h2>

	<div class="wp-filter" style="margin: 0;">
		<ul class="nav nav-tabs">
			<li><a href="#"><strong>Период</strong></a></li>

			<li <?php if($date == 'quart') echo 'class="active"' ?>>
				<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => 'quart')); ?>">квартал</a>
			</li>
			<li <?php if($date == 'month') echo 'class="active"' ?>>
				<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => 'month')); ?>">месяц</a>
			</li>
			<li <?php if($date == 'week') echo 'class="active"' ?>>
				<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => 'week')); ?>">неделя</a>
			</li>
		</ul>
	</div>

    <div class="postbox-container" style="width: 100%">
        <div class="metabox-holder">
            <div class="meta-box-sortables">
			
                <div class="postbox" id="first">
                    <div class="inside">
						<div id="piechart" style="width: 50%; float: left;"></div>
						<div id="datachart" style="width: 50%; float: left;"></div>
						<div style="clear: both"></div>
                    </div>
                </div>
				

				<table class="items table">
					<thead>
						<tr>
							<th class="manage-column column-title"><a>Пол посетителей</a></th>
							<th class="manage-column column-author">Доля визитов</th>
							<th class="manage-column column-author">Отказы</th>
							<th class="manage-column column-author">Глубина просмотра</th>
							<th class="manage-column column-author">Среднее время</th>
						</tr>
					</thead>

					<tbody>
						<?php

						foreach($json_data['data_gender'] as $key => $value) { 
							
							$name = $json_data['data_gender'][$key]['name'];
							$visits_percent = $json_data['data_gender'][$key]['visits_percent'];
							$denial = $json_data['data_gender'][$key]['denial'];
							$depth = $json_data['data_gender'][$key]['depth'];
							$visit_time = $json_data['data_gender'][$key]['visit_time'];
							
							$visit_time	= $visit_time/60;
						?>
						 
						<tr>
						  <th class="manage-column column-title"><a><?php echo $name; ?></a></th>
						  <th class="manage-column column-author"><?php echo round($visits_percent, 2); ?> %</th>
						  <th class="manage-column column-author"><?php echo $denial; ?></th>
						  <th class="manage-column column-author"><?php echo $depth; ?></th>
						  <th class="manage-column column-author"><?php echo round($visit_time, 1); ?></th>
						</tr>
						<?php } ?>
					</tbody>
				</table>                        

				<br>

				<table class="items table">
					<thead>
						<tr>
							<th class="manage-column column-title"><a>Возрастная группа</a></th>
							<th class="manage-column column-author">Доля визитов</th>
							<th class="manage-column column-author">Отказы</th>
							<th class="manage-column column-author">Глубина просмотра</th>
							<th class="manage-column column-author">Среднее время</th>
						</tr>
					</thead>

					<tbody>
						<?php
						foreach($json_data['data'] as $key => $value) { 
							
							$name = $json_data['data'][$key]['name'];
							$visits_percent = $json_data['data'][$key]['visits_percent'];
							$denial = $json_data['data'][$key]['denial'];
							$depth = $json_data['data'][$key]['depth'];
							$visit_time = $json_data['data'][$key]['visit_time'];
							
							$visit_time	= $visit_time/60;
						?>
						 
						<tr>
						  <th class="manage-column column-title"><a><?php echo $name; ?></a></th>
						  <th class="manage-column column-author"><?php echo round($visits_percent, 2); ?> %</th>
						  <th class="manage-column column-author"><?php echo $denial; ?></th>
						  <th class="manage-column column-author"><?php echo $depth; ?></th>
						  <th class="manage-column column-author"><?php echo round($visit_time, 1); ?></th>
						</tr>
						<?php } ?>
					</tbody>
				</table>				
            </div>
        </div>
    </div>

</div>