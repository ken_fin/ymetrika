<?php

	$date = isset($_GET['date']) ? $_GET['date'] : 'week';

	$date1 = $date;

	if($date1 == 'week') {		// если неделя
		$date1 = date('Ymd',strtotime("-7 day"));
	} elseif($date1 == 'month') {	// если месяц
		$date1 = date('Ymd',strtotime("-1 month"));
	} elseif($date1 == 'quart') {	// если квартал
		$date1 = date('Ymd',strtotime("-3 month"));
	} elseif($date1 == 'year') {	// если год
		$date1 = date('Ymd',strtotime("-12 month"));
	} else {
		$date1 = date('Ymd',strtotime("-1 month"));
	}

	$date2 = date('Ymd');

	$group = isset($_GET['group']) ? $_GET['group'] : 'week';

	if($group == 'day'){
		$group = 'day';
	} elseif($group == 'week') {
		$group = 'week';
	} elseif($group == 'month') {
		$group = 'month';
	} else {
		$group = 'day';
	}


	$url = 'https://api-metrika.yandex.ru/stat/traffic/load.json?id='.$dl_metrika_id.'&oauth_token='.$dl_token.'&date1='.$date1.'&date2='.$date2.'&group='.$group;
	$json_data = file_get_contents($url);
	$json_data = json_decode($json_data, true); 
?>


<script type="text/javascript">
      google.load("visualization", "1.1", {packages:["bar"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['', 'Запросы в секунду (максимум)', 'Посетители онлайн (максимум)'],
<?php
	foreach($json_data['data'] as $key => $value) { 
		
		$__date = date('d.m.y',strtotime($json_data['data'][$key]['date']));
		$max_rps = $json_data['data'][$key]['max_rps'];
		$max_users = $json_data['data'][$key]['max_users'];
		
		echo '[\''. $__date .'\','.$max_rps.','.$max_users.'],';

	}
?>
        ]);

         var options = {
          chart: {
            title: 'Данные о максимальном количестве запросов (срабатываний счетчика) в секунду и максимальное количество посетителей онлайн'
          }
        };

        var chart = new google.charts.Bar(document.getElementById('visites_div'));

        chart.draw(data, options);
		
	}
</script>
	
<div class="wrap">
	
	<h2>Отчет Нагрузка на сайт <a href="https://metrika.yandex.ru/legacy/load?id=<?php echo $dl_metrika_id; ?>" target="_blank" style="float: right" class="button">Отчет на Yandex.Metrika</a></h2>

	<div class="wrap">

		<div class="wp-filter" style="margin: 0;">
			<ul class="nav nav-tabs">
				<li><a href="#"><strong>Период</strong></a></li>	
				
				<li <?php if($date == 'quart') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => 'quart', 'group' => $group)); ?>">квартал</a>
				</li>
				<li <?php if($date == 'month') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => 'month', 'group' => $group)); ?>">месяц</a>
				</li>
				<li <?php if($date == 'week') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => 'week', 'group' => $group)); ?>" >неделя</a>
				</li>
				
				<li><a href="#"><strong>Детализация</strong></a></li>	
				
				<li <?php if($group == 'day') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => $date, 'group' => 'day')); ?>">по дням</a>
				</li>
				<li <?php if($group == 'week') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => $date, 'group' => 'week')); ?>">по неделям</a>
				</li>	
				<li <?php if($group == 'month') echo 'class="active"' ?>>
					<a href="<?php echo Yii::app()->createUrl(Yii::app()->request->requestUri, array('date' => $date, 'group' => 'month')); ?>">по месяцам</a>
				</li>	
			</ul>
		</div>

	    <div class="postbox-container" style="width: 100%">
	        <div class="metabox-holder">
	            <div class="meta-box-sortables">
				
				
                <div class="postbox" id="first">
                    <div class="inside">
						<div id="visites_div" style="width: 98%; height: 250px;"></div>
                    </div>
                </div>
					

				<table class="items table">
					<thead>
						<tr>
							<th class="manage-column column-title"><a>Дата</a></th>
							<th class="manage-column column-author">Запросы в секунду (максимум)</th>
							<th class="manage-column column-author">Посетители онлайн (максимум)</th>
						</tr>
					</thead>

					<tbody>
					<?php
					foreach($json_data['data'] as $key => $value) { 
						$traffic_date		= $json_data['data'][$key]['date'];
						$traffic_max_rps	= $json_data['data'][$key]['max_rps'];
						$traffic_max_users	= $json_data['data'][$key]['max_users'];
					?>  
					<tr>
					  <th class="manage-column column-title"><a><?php echo date('Y.m.d',strtotime($traffic_date)); ?></a></th>
					  <th class="manage-column column-author"><?php echo $traffic_max_rps; ?></th>
					  <th class="manage-column column-author"><?php echo $traffic_max_users; ?></th>
					</tr>
					<?php } ?>
					</tbody>
				</table>                        

	            </div>
	        </div>
	    </div>
	</div>
</div>