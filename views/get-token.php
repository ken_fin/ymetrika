<div class="wrap">
<h2>Настройка DL Yandex Metrika</h2>
<form method="post" action="">

<?php if($token == ''): ?>

<div class="form-group">
	<label class="input">
	Шаг 1: Получите свой токен Яндекс Метрики
	<a class="btn btn-info" target="_blank" href="https://oauth.yandex.ru/authorize?response_type=token&amp;client_id=d8de135c2b1045edb2199b258b9f23f0" class="button" style="margin-top:-5px">
		Разрешить доступ к своим данным и получить токен
	</a>
	</label>
</div>

<div class="form-group">
	<label class="input">
		Шаг 2: Ведите полученный "токен" и нажмите кнопку "Сохранить и продолжить".
		<input placeholder="Token доступа" class="form-control" type="text"	name="dl_yandex_metrika_token" value="<?php echo $token; ?>"  required>
	</label> 
</div>

<p class="form-group">
	<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Сохранить и продолжить</button>
</p>

<?php elseif($metrika_id == '') : ?>

<p><strong>Почти все готово, остался один шаг.</strong>

	<div class="form-group">
	<p>Выберите сайт из списка</p>
	<?php
	
	$url_counters = file_get_contents('https://api-metrika.yandex.ru/counters.json?oauth_token=' . $token);
	$json_data = json_decode($url_counters, true);
	?>
	
	<select name="dl_yandex_metrika_id" class="select2">
	<?php foreach($json_data['counters'] as $key => $value): ?>
	
	<?php
		$site_name = $json_data['counters'][$key]['site'];
		$site_id = $json_data['counters'][$key]['id'];
	?>
		<option
			<?php if ( $metrika_id == $site_id ) echo 'selected="selected"'; ?>  
			value="<?php echo $site_id ?>"><?php echo $site_name; ?></option>
	<?php endforeach; ?>
	
	</select>
	</div>
	<input 
		type="hidden" 
		name="dl_yandex_metrika_token" 
		value="<?php echo $token; ?>">

	<p class="submit">
		<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Завершить настройку</button>
	</p>

<?php endif; ?>
</form>
</div>