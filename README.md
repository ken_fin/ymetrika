# YMetrika Dashboard

Расширение для Yii - панель Yandex Метрика

Виджеты адаптированы с WordPress плагина ["DL Yandex Metrika"](https://wordpress.org/plugins/dl-yandex-metrika/)
разработанного [DyadyaLesha](http://vcard.dd-l.name/)

## Скрин

![screen](https://dl.dropboxusercontent.com/u/76506086/github/YMetrika/screen.jpg)

## Пример использования:

Простое подключение:

```php
<?php $this->widget('ext.YMetrika.YMetrika', array(
            'metrika_id' => $metrika_id, 	// Yii::app()->params['YM']->metrika_id
            'token' => $token,				// Yii::app()->params['YM']->token
            'widgets' => array(				// перечень выводимых виджетов
                'dashboard',
                'demography',
                'mobile'
            )
        )); 
?>
```

Доступные виджеты:

* dashboard - сводка за неделю, новые посетители
* demography - отчет по полу и возрасту
* geo - отчет по странам мира
* mobile - отчет по мобильным ОС
* os - отчет по ОС
* traffic - посещаемость
* traffic-load - нагрузка


## Дополнительно

Более гибкий способ применения можно посмотреть [здесь](./cooking.md).