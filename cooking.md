# Пример использования

Более гибкий путь использования расширения YMetrika - персистентные переменные.

К примеру, имеем класс PVariable:

```php
<?php
	class PVariable {
		public static function getValue($name, $default_value){
			/* получение значений из БД */
		}
		public static function setValue($name, $value){
			/* установка значений в БД */
		}
	}
```

Управление реализовано следующим образом:

```php
<?php
	
	class MetrikaController extends Controller {

		public function actionStat() {

			$token = PVariable::getValue('token', null);
			$metrika_id = PVariable::getValue('metrika_id', null);

			// настройка переменных
			if(isset($_POST['dl_metrika_token'])) {
				PVariable::setValue('token', $_POST['dl_metrika_token']);
			}
			if(isset($_POST['dl_metrika_metrika'])) {
				PVariable::setValue('token', $_POST['dl_metrika_token']);
			}

			// очистка переменных
			if(isset($_POST['dl_yandex_metrika_clear'])) {
				PVariable::setValue('token', null);
				PVariable::setValue('metrika_id', null);
			}

			$this->render('metrika', array(
				'token' => $token,
				'metrika_id' => $metrika_id
			));

		}

	}
```

ОК, теперь составим view файл:

```php

    <h1>Данные Яндекс Метрика</h1>
   
    <?php $page = isset($_GET['page']) ? $_GET['page'] : 'dashboard'; ?>

    <ul class="nav nav-tabs">
        <li><a href="#"><strong>Отчеты</strong></a></li>
        <li <?php if($page == 'dashboard') echo 'class="active"' ?>><a href="<?php echo $this->createUrl('yandexMetrika', array('page' => 'dashboard')); ?>">Консоль</a></li>
        <li <?php if($page == 'demography') echo 'class="active"' ?>><a href="<?php echo $this->createUrl('yandexMetrika', array('page' => 'demography')); ?>">Отчет по полу и возрасту</a></li>
        <li <?php if($page == 'geo') echo 'class="active"' ?>><a href="<?php echo $this->createUrl('yandexMetrika', array('page' => 'geo')); ?>">Отчет по странам мира</a></li>
        <li <?php if($page == 'mobile') echo 'class="active"' ?>><a href="<?php echo $this->createUrl('yandexMetrika', array('page' => 'mobile')); ?>">Мобильные ОС</a></li>
        <li <?php if($page == 'os') echo 'class="active"' ?>><a href="<?php echo $this->createUrl('yandexMetrika', array('page' => 'os')); ?>">ОС</a></li>
        <li <?php if($page == 'traffic') echo 'class="active"' ?>><a href="<?php echo $this->createUrl('yandexMetrika', array('page' => 'traffic')); ?>">Посещаемость</a></li>
        <li <?php if($page == 'traffic-load') echo 'class="active"' ?>><a href="<?php echo $this->createUrl('yandexMetrika', array('page' => 'traffic-load')); ?>">Нагрузка на сайт</a></li>
    </ul>

    <?php $this->widget('ext.YMetrika.YMetrika', array(
        'metrika_id' => $metrika_id,
        'token' => $token,
        'widgets' => array(
            $page
        )
    )); ?>
	
	<!-- Сброс настроек -->
	<div>
	    <form action="" method="POST">
	        <button name="dl_yandex_metrika_clear">
	            Сбросить настройки метрики
	        </button>
	    </form>
    </div>
```

Таким образом, сперва пользователь будет проведен через шаги установки расширения, а затем сможет просматривать каждый экран метрики отдельно.