<?php

	/*
		Yandex Metrika dashboard widget.

		Widgets are adapted from WordPress plugin "DL Yandex Metrika" (https://wordpress.org/plugins/dl-yandex-metrika/)
		developed by DyadyaLesha (http://vcard.dd-l.name/)

		@author ikenfin (ikenfin@gmail.com)
		@home i.kenfin.ru

		2015
	*/

	/*
		Usage example:

		<?php $this->widget('admin.widgets.YMetrika.YMetrika', array(
            'metrika_id' => $metrika_id,
            'token' => $token,
            'widgets' => array(
                'dashboard',
                'demography',
                'mobile'
            )
        )); ?>
	*/

	class YMetrika extends CWidget {

		// settings
		public $metrika_id, $token;

		// available widgets
		public $widgets = array(
			'dashboard',
			'demography',
			'geo',
			'mobile',
			'os',
			'traffic',
			'traffic-load'
		);

		public function run() {
			if($this->metrika_id == null || $this->token == null) {
				// teach user how to get token
				$this->render('get-token', array(
					'metrika_id' => $this->metrika_id,
					'token' => $this->token
				));

				return;
			}

			Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/jsapi', CClientScript::POS_HEAD);

			foreach($this->widgets as $widget) {
				$this->render('widgets/y-' . $widget, array(
					'dl_metrika_id' => $this->metrika_id,
					'dl_token' => $this->token
				));
			}
		}

	}